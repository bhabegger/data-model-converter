package tech.habegger.datamodel.metamodel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class Model {
    LinkedHashMap<String, EntityType> entityTypes = new LinkedHashMap<>();
    LinkedHashMap<String, ScalarType> scalarTypes = new LinkedHashMap<>();
    LinkedHashMap<String, EnumType> enumTypes = new LinkedHashMap<>();

    List<Relationship> relationships = new ArrayList<>();
    Set<EntityType> topLevel = new LinkedHashSet<>();

    public EntityType getOrCreateEntityType(String name) {
        return entityTypes.computeIfAbsent(name, EntityType::new);
    }

    public ScalarType getOrCreateScalarType(String name) {
        return scalarTypes.computeIfAbsent(name, ScalarType::new);
    }

    public void createEnumType(String name, List<String> values) {
        enumTypes.put(name, new EnumType(name, values));
    }

    public EnumType getEnumType(String simpleName) {
        return enumTypes.get(simpleName);
    }

    public void addRelationship(EntityType entityType, String name, Cardinality cardinality, Type fieldEntityType) {
        Relationship rel = new Relationship(entityType, name, cardinality, fieldEntityType);
        entityType.addOutgoingRelationship(rel);
        relationships.add(rel);
    }

    public Collection<EntityType> getEntityTypes() {
        return entityTypes.values();
    }

    public EntityType getEntityType(String name) {
        return entityTypes.get(name);
    }

    public Collection<ScalarType> getScalarTypes() {
        return scalarTypes.values();
    }

    public Collection<EnumType> getEnumTypes() {
        return enumTypes.values();
    }

    public void addTopLevelEntity(EntityType topLevel) {
        this.topLevel.add(topLevel);
    }


}
