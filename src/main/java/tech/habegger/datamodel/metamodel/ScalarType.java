package tech.habegger.datamodel.metamodel;

public record ScalarType(String name) implements Type {
    @Override
    public String getName() {
        return name;
    }
}
