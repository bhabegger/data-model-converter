package tech.habegger.datamodel.metamodel;

import java.util.List;

public record EnumType(String name, List<String> values) implements Type {
    @Override
    public String getName() {
        return name;
    }
}
