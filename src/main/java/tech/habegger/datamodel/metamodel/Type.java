package tech.habegger.datamodel.metamodel;

public interface Type {
    String getName();
}
