package tech.habegger.datamodel.metamodel;

public enum Cardinality {
    ZERO_ONE,
    ONE,
    ZERO_MANY,
    ONE_MANY
}
