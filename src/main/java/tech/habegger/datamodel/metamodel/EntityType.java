package tech.habegger.datamodel.metamodel;

import java.util.ArrayList;
import java.util.List;

public class EntityType implements Type {
    private final String name;
    private final List<Relationship> outgoingRelationShips;

    public EntityType(String name) {
        this.name = name;
        this.outgoingRelationShips = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public List<Relationship> getOutgoingRelationships() {
        return outgoingRelationShips;
    }

    public void addOutgoingRelationship(Relationship rel) {
        outgoingRelationShips.add(rel);
    }

    @Override
    public String toString() {
        return name;
    }
}
