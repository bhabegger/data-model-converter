package tech.habegger.datamodel.metamodel;

public record FieldReference(String type, String name) {
}
