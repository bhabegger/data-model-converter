package tech.habegger.datamodel.metamodel;

public record Relationship(Type source, String name, Cardinality cardinality, Type target) {

}
