package tech.habegger.datamodel.converter.elastic;

public enum ElasticFieldOption {
    SKIP,
    EMBED,
    JOIN
}
