package tech.habegger.datamodel.converter.elastic;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import tech.habegger.datamodel.converter.ModelReader;
import tech.habegger.datamodel.metamodel.Cardinality;
import tech.habegger.datamodel.metamodel.EntityType;
import tech.habegger.datamodel.metamodel.Model;
import tech.habegger.datamodel.metamodel.Type;
import tech.habegger.elastic.mapping.ElasticFieldProperty;
import tech.habegger.elastic.mapping.ElasticObjectProperty;
import tech.habegger.elastic.mapping.ElasticSettings;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Map;

public class ElasticMappingModelReader implements ModelReader {
    private static final ObjectMapper MAPPER = new ObjectMapper();
    @Override
    public Model read(String sourceLocation) throws IOException {
        URL url = new URL(sourceLocation);
        try(InputStream is = url.openStream()) {
            Map<String, ElasticSettings> settings = MAPPER.readValue(is, new TypeReference<Map<String, ElasticSettings>>() {});
            return read(settings);
        }
    }

    public Model read(Map<String, ElasticSettings> settings) {
        Model model = new Model();
        settings.forEach((key, value) -> {
            var topLevel = populateObjectDefinition(model, ElasticModelHelper.capitalize(key), value.mappings());
            model.addTopLevelEntity(topLevel);
        });
        return model;
    }

    private EntityType populateObjectDefinition(Model model, String objectName, ElasticObjectProperty mappings) {
        EntityType entityType = model.getOrCreateEntityType(objectName);
        mappings.properties().forEach((propertyName, propertyDefinition) -> {
            Type fieldType;
            if (propertyDefinition instanceof ElasticObjectProperty objectPropertyDefinition) {
                fieldType = populateObjectDefinition(model, objectName + ElasticModelHelper.capitalize(propertyName), objectPropertyDefinition);
            } else if (propertyDefinition instanceof ElasticFieldProperty fieldPropertyDefinition) {
                fieldType = modelTypeOfElasticType(model, fieldPropertyDefinition.type());
            } else {
                throw new RuntimeException("Don't know how to handle %s".formatted(propertyDefinition.getClass().getSimpleName()));
            }
            model.addRelationship(entityType, propertyName, Cardinality.ZERO_ONE, fieldType);
        });
        return entityType;
    }

    private Type modelTypeOfElasticType(Model model, String type) {
        return switch(type) {
            case "keyword", "text" -> model.getOrCreateScalarType("String");
            case "long", "unsigned_long" -> model.getOrCreateScalarType("Integer");
            default -> model.getOrCreateScalarType(ElasticModelHelper.capitalize(type));
        };
    }

}

