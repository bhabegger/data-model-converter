package tech.habegger.datamodel.converter.elastic;

public class ElasticModelHelper {
    public static String capitalize(String input) {
        return input.substring(0, 1).toUpperCase() + input.substring(1);
    }
}
