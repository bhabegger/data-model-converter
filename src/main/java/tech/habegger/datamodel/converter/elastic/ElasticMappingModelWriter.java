package tech.habegger.datamodel.converter.elastic;

import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import tech.habegger.datamodel.converter.ModelWriter;
import tech.habegger.datamodel.metamodel.EntityType;
import tech.habegger.datamodel.metamodel.FieldReference;
import tech.habegger.datamodel.metamodel.Model;
import tech.habegger.datamodel.metamodel.ScalarType;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static tech.habegger.datamodel.converter.elastic.ElasticFieldOption.EMBED;
import static tech.habegger.datamodel.converter.elastic.ElasticFieldOption.JOIN;
import static tech.habegger.datamodel.converter.elastic.ElasticFieldOption.SKIP;

public class ElasticMappingModelWriter implements ModelWriter {

    private static final Map<String, String> SCALAR_TO_FIELD_TYPEMAP = Map.of(
    "LocalDate", "date"
    );
    Map<FieldReference, ElasticFieldOption> fieldOptions = new HashMap<>();

    public ElasticMappingModelWriter embed(String type, String name) {
        fieldOptions.put(new FieldReference(type, name), EMBED);
        return this;
    }

    public ElasticMappingModelWriter skip(String type, String name) {
        fieldOptions.put(new FieldReference(type, name), SKIP);
        return this;
    }

    public ElasticMappingModelWriter join(String type, String name) {
        fieldOptions.put(new FieldReference(type, name), JOIN);
        return this;
    }

    @Override
    public void write(Model model, String targetLocation) throws IOException {
        File baseDirectory = new File(targetLocation);
        var ignored = baseDirectory.mkdirs();
        for(var entity: model.getEntityTypes()) {
            generateEntityMappingTemplate(entity, baseDirectory);
        }
    }
    private void generateEntityMappingTemplate(EntityType entity, File baseDirectory) throws IOException {
        String entityNameLowerCase = entity.getName().toLowerCase();
        File entityMappingFile = new File(baseDirectory, entityNameLowerCase + ".json");

        JsonFactory factory = new JsonFactory();
        try(JsonGenerator generator = factory.createGenerator(entityMappingFile, JsonEncoding.UTF8).useDefaultPrettyPrinter()) {
            generator.writeStartObject();
            generator.writeObjectFieldStart("template");
            generator.writeObjectFieldStart("mappings");
            generator.writeObjectFieldStart("properties");
            generator.writeObjectFieldStart(entityNameLowerCase);
            generator.writeObjectFieldStart("properties");

            generateEntityContent(generator, entity);

            generator.writeEndObject();
            generator.writeEndObject();
            generator.writeEndObject();
            generator.writeEndObject();
            generator.writeEndObject();
            generator.writeEndObject();
        }
    }

    private void generateEntityContent(JsonGenerator generator, EntityType entity) throws IOException {
        Map<String, String> relationships = new HashMap<>();
        for(var rel : entity.getOutgoingRelationships()) {
            var targetType = rel.target();
            if(targetType instanceof ScalarType scalarType) {
                generator.writeObjectFieldStart(rel.name());
                String fieldType = determineScalarFieldType(scalarType);
                generator.writeStringField("type", fieldType);
                generator.writeEndObject();
            } else if(targetType instanceof EntityType targetEntity){
                FieldReference reference = new FieldReference(entity.getName(), rel.name());
                ElasticFieldOption option = fieldOptions.get(reference);

                switch (option) {
                    case EMBED -> {
                        generator.writeObjectFieldStart(rel.name());
                        generator.writeObjectFieldStart("properties");
                        generateEntityContent(generator, targetEntity);
                        generator.writeEndObject();
                        generator.writeEndObject();
                    }
                    case JOIN -> relationships.put(rel.name(), rel.name() +"_inv");
                    case SKIP -> {}
                }
            }
        }
        if(!relationships.isEmpty()) {
            generator.writeObjectFieldStart("__relationships");
            generator.writeStringField("type", "join");
            generator.writeObjectFieldStart("relations");
            for(var entry: relationships.entrySet()) {
                generator.writeStringField(entry.getKey(), entry.getValue());
            }
            generator.writeEndObject();
            generator.writeEndObject();
        }
    }

    private String determineScalarFieldType(ScalarType scalarType) {
        return SCALAR_TO_FIELD_TYPEMAP.getOrDefault(scalarType.name(), "keyword");
    }
}

