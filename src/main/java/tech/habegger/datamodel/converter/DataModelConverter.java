package tech.habegger.datamodel.converter;

import org.apache.commons.cli.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.util.Arrays;
import java.util.Properties;

public class DataModelConverter {

    private final static Option HELP = Option.builder("h").longOpt("help").desc("Display this help message").build();
    private final static Option VERSION = Option.builder("v").longOpt("version").desc("Display version").build();

    private final static Option SOURCE = Option.builder("s").longOpt("source").argName("source").hasArg().desc("Define the type of source model").build();
    private final static Option TARGET = Option.builder("s").longOpt("target").argName("target").hasArg().desc("Define the type of target model").build();

    private static final Options OPTIONS = new Options();
    static {
        OPTIONS.addOption(HELP);
        OPTIONS.addOption(VERSION);
        OPTIONS.addOption(SOURCE);
        OPTIONS.addOption(TARGET);
        OPTIONS.addOption(SOURCE);
    }

    public static void main(String[] args) throws ParseException, IOException {
        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = parser.parse(OPTIONS, args);

        if(cmd.hasOption(VERSION)) {
            printVersion();
            return;
        }

        if(cmd.hasOption(HELP)) {
            printUsage();
            return;
        }

        ModelReader sourceReader = null;
        if(cmd.hasOption(SOURCE)) {
            String rawSource = cmd.getOptionValue(SOURCE);
            try {
                ModelType source = ModelType.valueOf(rawSource.toUpperCase());
                sourceReader = ModelReader.forSource(source);
            } catch(IllegalArgumentException e) {
                println("Unknown model type: %s".formatted(rawSource));
                displayReadableModels();
                return;
            }
        }

        ModelWriter targetWriter = null;
        if(cmd.hasOption(SOURCE)) {
            String rawTarget = cmd.getOptionValue(TARGET);
            try {
                ModelType target = ModelType.valueOf(rawTarget.toUpperCase());
                targetWriter = ModelWriter.forTarget(target);
            } catch(IllegalArgumentException e) {
                println("Unknown model type: %s".formatted(rawTarget));
                displayWritableModels();
                return;
            }
            if(targetWriter == null) {
                println("Cannot write models of type: %s".formatted(rawTarget));
                displayWritableModels();
                return;
            }
        }
        if(cmd.getArgList().size() == 2) {
            String sourceLocation = cmd.getArgList().get(0);
            assert sourceReader != null;
            String targetLocation = cmd.getArgList().get(1);
            assert targetWriter != null;
            targetWriter.write(sourceReader.read(sourceLocation), targetLocation);
        } else {
            println("Invalid arguments");
            printUsage();
        }

    }

    private static void displayReadableModels() {
        println("Known readable model types are: ");
        Arrays.stream(ModelType.values()).forEach(
            it -> {
                if(ModelReader.forSource(it) != null) {
                    println(" - %s".formatted(it.toString()));
                }
            }
        );
    }

    private static void displayWritableModels() {
        println("Known writable model types are: ");
        Arrays.stream(ModelType.values()).forEach(
            it -> {
                if(ModelWriter.forTarget(it) != null) {
                    println(" - %s".formatted(it.toString()));
                }
            }
        );
    }
    private static void printUsage() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("data-model-converter [OPTIONS] [INPUT [OUTPUT]]", OPTIONS);
    }

    private static void printVersion() {
        try(InputStream is = DataModelConverter.class.getResourceAsStream("version.properties")) {
            Properties props = new Properties();
            props.load(is);
            String version = props.getProperty("version");
            println("Version: %s".formatted(version));
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    private static void println(String output) {
        System.out.println(output);
    }

}
