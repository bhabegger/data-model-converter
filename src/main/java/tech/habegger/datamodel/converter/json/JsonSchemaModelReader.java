package tech.habegger.datamodel.converter.json;

import com.networknt.schema.*;
import tech.habegger.datamodel.converter.ModelReader;
import tech.habegger.datamodel.metamodel.Cardinality;
import tech.habegger.datamodel.metamodel.EntityType;
import tech.habegger.datamodel.metamodel.Model;
import tech.habegger.datamodel.metamodel.Type;

import java.util.HashMap;
import java.util.Map;

public class JsonSchemaModelReader implements ModelReader {
    private static final Map<String, String> SCALAR_MAP = Map.of("string", "String");
    @Override
    public Model read(String sourceLocation) {
        Model model = new Model();
        Map<String, TypeWithCardinality> schemaRefCache = new HashMap<>();
        JsonSchemaFactory factory = JsonSchemaFactory.getInstance(SpecVersion.VersionFlag.V4);
        JsonSchema jsonSchema = factory.getSchema(JsonSchemaModelReader.class.getResourceAsStream(sourceLocation));
        populateModel(jsonSchema, model, schemaRefCache);
        return model;
    }

    private TypeWithCardinality populateModel(JsonSchema jsonSchema, Model model, Map<String, TypeWithCardinality> schemaRefCache) {
        TypeValidator typeValidator = null;
        PropertiesValidator propertiesValidator = null;
        RefValidator refValidator = null;
        ItemsValidator itemsValidator = null;
        String title = null;
        var validators = jsonSchema.getValidators();
        for(var validator: validators) {
            if(validator instanceof TypeValidator x) {
                typeValidator = x;
            } else if(validator instanceof PropertiesValidator x) {
                propertiesValidator = x;
            } else if(validator instanceof AbstractJsonValidator x && "title".equals(validator.getKeyword())) {
                title = x.getSchemaNode().asText();
            } else if(validator instanceof RefValidator x) {
                refValidator = x;
            } else if(validator instanceof ItemsValidator x) {
                itemsValidator = x;
            }
        }

        if(refValidator != null) {
            var ref = refValidator.getSchemaRef();
            var schemaNode = refValidator.getSchemaNode().asText();
            if(schemaRefCache.containsKey(schemaNode)) {
                return schemaRefCache.get(schemaNode);
            } else {
                TypeWithCardinality result = populateModel(ref.getSchema(), model, schemaRefCache);
                schemaRefCache.put(schemaNode, result);
                return result;
            }
        } else {
            assert typeValidator != null;
            if (propertiesValidator == null) {
                if(itemsValidator == null) {
                    if (title == null) {
                        title = typeValidator.getSchemaType().toString();
                        if(SCALAR_MAP.containsKey(title)) {
                            title = SCALAR_MAP.get(title);
                        }
                    }
                    return new TypeWithCardinality(model.getOrCreateScalarType(title), Cardinality.ZERO_ONE);
                } else {
                    TypeWithCardinality rawType = populateModel(itemsValidator.getSchema(), model, schemaRefCache);
                    return new TypeWithCardinality(rawType.type, Cardinality.ZERO_MANY);
                }
            } else {
                EntityType type = model.getEntityType(title);
                if(type == null) {
                    type = model.getOrCreateEntityType(title);
                    for (var property : propertiesValidator.getSchemas().entrySet()) {
                        TypeWithCardinality child = populateModel(property.getValue(), model, schemaRefCache);
                        model.addRelationship(type, property.getKey(), child.cardinality, child.type);
                    }
                }
                return new TypeWithCardinality(type, Cardinality.ZERO_ONE);
            }
        }
    }

    record TypeWithCardinality(Type type, Cardinality cardinality) {}
}
