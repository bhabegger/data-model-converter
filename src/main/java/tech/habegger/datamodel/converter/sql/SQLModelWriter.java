package tech.habegger.datamodel.converter.sql;

import tech.habegger.datamodel.converter.ModelWriter;
import tech.habegger.datamodel.metamodel.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class SQLModelWriter implements ModelWriter {

    private static final Map<String, String> SCALAR_MAP = Map.of(
        "String", "VARCHAR(256)",
        "LocalDate", "DATE"
    );
    private static final String ENUM_TYPE = "VARCHAR(128)";

    @Override
    public void write(Model model, String targetLocation) throws IOException {
        File targetFile = new File(targetLocation);
        try (FileWriter writer = new FileWriter(targetFile)) {
            write(model, writer);
        }
    }

    public void write(Model model, Writer writer) throws IOException {
        LinkedHashMap<String, TableDefinition> relModel = convert(model);
        write(relModel, writer);

    }

    private LinkedHashMap<String, TableDefinition> convert(Model model) {
        LinkedHashMap<String, TableDefinition> result = new LinkedHashMap<>();
        convert(model, result);
        return result;
    }

    private void convert(Model model, LinkedHashMap<String, TableDefinition> result) {
        for(var entity: model.getEntityTypes()) {
            convert(entity, result);
        }
    }

    private void convert(EntityType entity, LinkedHashMap<String, TableDefinition> result) {
        List<Column> columns = new ArrayList<>();
        columns.add(new Column("id", "INTEGER", true, false));
        List<ForeignReference> foreignReferences = new ArrayList<>();
        for(var rel: entity.getOutgoingRelationships()) {
                switch (rel.cardinality()) {
                    case ZERO_ONE, ONE -> {
                        if(rel.target() instanceof ScalarType scalarTarget) {
                            columns.add(new Column(scalarColumnName(rel.name()), sqlTypeOf(scalarTarget), false, isNullable(rel.cardinality())));
                        } else if(rel.target() instanceof EnumType enumType) {
                            columns.add(new Column(scalarColumnName(rel.name()), sqlTypeOf(enumType), false, isNullable(rel.cardinality())));
                        } else {
                            var columnName = foreignColumnName(rel.name());
                            foreignReferences.add(new ForeignReference(columnName, tableName(rel.target().getName())));
                            columns.add(new Column(columnName, "INTEGER", false, isNullable(rel.cardinality())));
                        }
                    }
                    case ZERO_MANY, ONE_MANY -> {
                        var inColumn = tableName(rel.name()) + "_in";
                        var outColumn = tableName(rel.name()) + "_out";
                        var tableName = linkTableName(entity.getName(), rel.name());

                        if(rel.target() instanceof ScalarType scalarTarget) {
                            List<Column> linkColumns = List.of(
                                new Column(inColumn, "INTEGER", true, false),
                                new Column(outColumn, sqlTypeOf(scalarTarget), true, isNullable(rel.cardinality()))
                            );
                            List<ForeignReference> linkForeignKeys = List.of(
                                new ForeignReference(inColumn, tableName(entity.getName()))
                            );
                            result.put(tableName, new TableDefinition(linkColumns, linkForeignKeys));
                        } else if(rel.target() instanceof EnumType enumType) {
                            List<Column> linkColumns = List.of(
                                    new Column(inColumn, "INTEGER", true, false),
                                    new Column(outColumn, sqlTypeOf(enumType), true, isNullable(rel.cardinality()))
                            );
                            List<ForeignReference> linkForeignKeys = List.of(
                                    new ForeignReference(inColumn, tableName(entity.getName()))
                            );
                            result.put(tableName, new TableDefinition(linkColumns, linkForeignKeys));
                        } else{
                            List<Column> linkColumns = List.of(
                                new Column(inColumn, "INTEGER", true, false),
                                new Column(outColumn, "INTEGER", true,false)
                            );
                            List<ForeignReference> linkForeignKeys = List.of(
                                new ForeignReference(inColumn, tableName(entity.getName())),
                                new ForeignReference(outColumn, tableName(rel.target().getName()))
                            );
                            result.put(tableName, new TableDefinition(linkColumns, linkForeignKeys));
                        }
                    }
                }
        }
        result.put(tableName(entity.getName()), new TableDefinition(columns, foreignReferences));
    }

    private String sqlTypeOf(EnumType ignored) {
        return ENUM_TYPE;
    }

    private static String tableName(String entityName) {
        return entityName.toLowerCase();
    }


    private static String foreignColumnName(String relName) {
        return relName.toLowerCase();
    }

    private static String linkTableName(String entityName, String relName) {
        return tableName(entityName) + "_" + relName.toLowerCase();
    }

    private static String scalarColumnName(String name) {
        return name.toLowerCase();
    }

    private static boolean isNullable(Cardinality cardinality) {
        return cardinality == Cardinality.ZERO_ONE || cardinality == Cardinality.ZERO_MANY;
     }

    private static String sqlTypeOf(ScalarType scalarType) {
        return SCALAR_MAP.getOrDefault(scalarType.getName(), "UNKNOWN");
    }

    private void write(LinkedHashMap<String, TableDefinition> tables, Writer output) throws IOException {
        for(var table: tables.entrySet()) {
            writeTableDefinition(table.getKey(), table.getValue(), output);
        }
        for(var table: tables.entrySet()) {
            writeTableReferences(table.getKey(), table.getValue(), output);
        }
    }

    private void writeTableDefinition(String name, TableDefinition table, Writer output) throws IOException {
        output.write("CREATE TABLE %s (\n".formatted(name));
        var first = true;
        List<String> primaryKey = new ArrayList<>();
        for(var column: table.columns()) {
            if(column.primary()) {
                primaryKey.add(column.name());
            }
            if(first) {
                first = false;
            } else {
                output.write(",\n");
            }
            output.write("\t%s %s%s".formatted(column.name(), column.type(), column.nullable() ? "" : " NOT NULL"));
        }
        if (!primaryKey.isEmpty()) {
            output.write(",\n\tPRIMARY KEY (%s)".formatted(String.join(", ", primaryKey)));
        }
        output.write("\n);\n");
    }

    private void writeTableReferences(String name, TableDefinition table, Writer output) throws IOException {
        for(var foreignKey: table.foreignReferences) {
            output.write(
                """
                ALTER TABLE %s ADD CONSTRAINT fk_%s_%s FOREIGN KEY (%s) REFERENCES %s(id);
                """.formatted(name, name, foreignKey.columnName, foreignKey.columnName, foreignKey.targetTable)
            );
        }
    }

    private record Column(String name, String type, boolean primary, boolean nullable) {
    }

    private record ForeignReference(String columnName, String targetTable) {

    }

    private record TableDefinition(List<Column> columns, List<ForeignReference> foreignReferences) {

    }
}
