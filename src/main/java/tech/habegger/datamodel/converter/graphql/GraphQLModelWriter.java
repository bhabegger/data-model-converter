package tech.habegger.datamodel.converter.graphql;

import tech.habegger.datamodel.converter.ModelWriter;
import tech.habegger.datamodel.metamodel.*;

import java.io.*;

public class GraphQLModelWriter implements ModelWriter {

    @Override
    public void write(Model model, String targetLocation) throws IOException {
        File targetFile = new File(targetLocation);
        try (FileWriter writer = new FileWriter(targetFile)) {
            write(model, writer);
        }
    }

    public void write(Model model, Writer output) throws IOException {
        for(var enums: model.getEnumTypes()) {
            writeEnum(enums, output);
        }
        for(var scalar: model.getScalarTypes()) {
            writeScalar(scalar, output);
        }
        for(var entity: model.getEntityTypes()) {
            writeEntity(entity, output);
        }
    }

    private void writeScalar(ScalarType scalar, Writer output) throws IOException {
        if(!GraphQLModelHelper.PREDEFINED_SCALARS.containsKey(scalar.getName())) {
            output.write("scalar %s\n".formatted(GraphQLModelHelper.graphQLSafeNameOf(scalar.getName())));
        }
    }


    private void writeEnum(EnumType entity, Writer output) throws IOException {
        var entityName = GraphQLModelHelper.graphQLSafeNameOf(entity.getName());
        output.write("enum %s {\n".formatted(entityName));
        for(var entry: entity.values()) {
            output.write("\t%s\n".formatted(entry));
        }
        output.write("}\n");
    }

    public void writeEntity(EntityType entity, Writer output) throws IOException {
        var entityName = GraphQLModelHelper.graphQLSafeNameOf(entity.getName());
        output.write("type %s {\n".formatted(entityName));
        for(var rel: entity.getOutgoingRelationships()) {
            writeEntityRelationship(rel, output);
        }
        output.write("}\n");
    }

    private void writeEntityRelationship(Relationship rel, Writer output) throws IOException {
        var relName = GraphQLModelHelper.graphQLSafeNameOf(rel.name());
        var targetName = GraphQLModelHelper.PREDEFINED_SCALARS.getOrDefault(
            rel.target().getName(),
            GraphQLModelHelper.graphQLSafeNameOf(rel.target().getName())
        );
        switch (rel.cardinality()) {
            case ZERO_ONE -> output.write("\t%s: %s\n".formatted(relName, targetName));
            case ONE -> output.write("\t%s: %s!\n".formatted(relName, targetName));
            case ZERO_MANY -> output.write("\t%s: [%s]\n".formatted(relName, targetName));
            case ONE_MANY -> output.write("\t%s: [%s!]!\n".formatted(relName, targetName));
        }
    }
}
