package tech.habegger.datamodel.converter.graphql;

import java.util.Map;
import java.util.Set;

public class GraphQLModelHelper {
    public static final Map<String, String> PREDEFINED_SCALARS = Map.of(
        "Integer", "Int",
        "Long", "Int",
        "Float", "Float",
        "Decimal", "Float",
        "String","String",
        "Boolean", "Boolean",
        "ID", "ID"
    );

    public static String graphQLSafeNameOf(String original) {
        // TODO manage name clashes
         var transformed = original.replaceAll("[^_0-9A-Za-z]","_");
         if(transformed.matches("^[0-9]")) {
             return "_" + transformed;
         } else {
             return transformed;
         }
    }
}
