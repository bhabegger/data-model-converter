package tech.habegger.datamodel.converter.graphql;

import graphql.language.*;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;
import tech.habegger.datamodel.converter.ModelReader;
import tech.habegger.datamodel.metamodel.*;
import tech.habegger.datamodel.metamodel.Type;

import java.io.*;

public class GraphQLModelReader implements ModelReader {

    private static final SchemaParser SCHEMA_PARSER = new SchemaParser();
    private TypeDefinitionRegistry typeRegistry;

    @Override
    public Model read(String sourceLocation) throws IOException {
        return read(schemaOfResource(sourceLocation));
    }

    public Model read(TypeDefinitionRegistry registry) {
        Model model = new Model();
        typeRegistry = registry;
        for(var typeDefinition: typeRegistry.types().values()) {
            addTypeDefinition(model, typeDefinition);
        }
        return model;
    }
    private void addTypeDefinition(Model model, TypeDefinition<?> typeDefinition) {
        if (typeDefinition instanceof ObjectTypeDefinition objectTypeDefinition) {
            EntityType entity = model.getOrCreateEntityType(objectTypeDefinition.getName());
            for(var fieldDefinition: objectTypeDefinition.getFieldDefinitions()) {
                addFieldDefinition(model, entity, fieldDefinition);
            }

        } else if(typeDefinition instanceof ScalarTypeDefinition scalarTypeDefinition) {
            model.getOrCreateScalarType(scalarTypeDefinition.getName());
        }
    }

    private void addFieldDefinition(Model model, EntityType entity, FieldDefinition fieldDefinition) {
        var graphqlFieldType =  fieldDefinition.getType();
        Cardinality cardinality;
        boolean isNonNull = false;
        if(graphqlFieldType instanceof NonNullType nonNullType) {
            isNonNull = true;
            graphqlFieldType = nonNullType.getType();
        }
        if(graphqlFieldType instanceof ListType listType) {
            cardinality = isNonNull ? Cardinality.ONE_MANY : Cardinality.ZERO_MANY;
            graphqlFieldType = listType.getType();
        } else {
            cardinality = isNonNull ? Cardinality.ONE : Cardinality.ZERO_ONE;
        }
        if(graphqlFieldType instanceof TypeName typeName) {
            Type modelFieldType;
            if (isScalarType(typeName)) {
                modelFieldType = model.getOrCreateScalarType(typeName.getName());
            } else {
                modelFieldType = model.getOrCreateEntityType(typeName.getName());
            }
            model.addRelationship(entity, fieldDefinition.getName(), cardinality, modelFieldType);
        }
    }

    private boolean isScalarType(graphql.language.Type<?> graphqlFieldType) {
        return typeRegistry.getType(graphqlFieldType).map(it -> it instanceof ScalarTypeDefinition).orElse(false);
    }

    public static TypeDefinitionRegistry schemaOfResource(String sourceLocation) throws IOException {
        try(InputStream is =  GraphQLModelReader.class.getResourceAsStream(sourceLocation)) {
            return SCHEMA_PARSER.parse(is);
        }
    }
    public static TypeDefinitionRegistry schemaOfString(String graphQLSchema) {
        return SCHEMA_PARSER.parse(graphQLSchema);
    }

}
