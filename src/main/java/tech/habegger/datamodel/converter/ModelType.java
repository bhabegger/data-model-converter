package tech.habegger.datamodel.converter;

public enum ModelType {
    POJO,
    GRAPHQL,
    ELASTIC,
    SQL,
    JSON_SCHEMA,
    XSD
}
