package tech.habegger.datamodel.converter;

import tech.habegger.datamodel.converter.java.JavaModelReader;
import tech.habegger.datamodel.metamodel.Model;

import java.io.IOException;

public interface ModelReader {
    static ModelReader forSource(ModelType source) {
        if(source == ModelType.POJO) {
            return new JavaModelReader();
        }
        return null;
    }

    Model read(String sourceLocation) throws IOException;
}
