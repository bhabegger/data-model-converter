package tech.habegger.datamodel.converter;

import tech.habegger.datamodel.converter.graphql.GraphQLModelWriter;
import tech.habegger.datamodel.metamodel.Model;

import java.io.IOException;

public interface ModelWriter {
    static ModelWriter forTarget(ModelType it) {
        if(it == ModelType.GRAPHQL) {
            return new GraphQLModelWriter();
        }
        return null;
    }

    void write(Model model, String targetLocation) throws IOException;
}
