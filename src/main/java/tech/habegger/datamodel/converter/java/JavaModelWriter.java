package tech.habegger.datamodel.converter.java;

import tech.habegger.datamodel.converter.ModelWriter;
import tech.habegger.datamodel.metamodel.EntityType;
import tech.habegger.datamodel.metamodel.Model;
import tech.habegger.datamodel.metamodel.Relationship;
import tech.habegger.datamodel.metamodel.Type;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Set;
import java.util.stream.Collectors;
public class JavaModelWriter implements ModelWriter {
    static File BASE_DIR = new File("target/generated-src/main/java");

    @Override
    public void write(Model model, String targetPackage) throws IOException {
        for(var entity: model.getEntityTypes()) {
            generateEntityClass(entity, targetPackage);
        }
    }
    private void generateEntityClass(EntityType entity, String targetPackage) throws IOException {
        File packageDir = new File(BASE_DIR, targetPackage.replaceAll("\\.", "/"));
        var ignored = packageDir.mkdirs();
        File classFile = new File(packageDir, entity.getName() + ".java");
        try(var writer = new FileWriter(classFile)) {
            generateClassContent(entity, targetPackage, writer);
        }
    }
    public void generateClassContent(EntityType entity, String targetPackage, Writer writer) throws IOException {
            writer.write("package %s;\n\n".formatted(targetPackage));

            writeImports(writer, entity.getOutgoingRelationships().stream().map(Relationship::target).collect(Collectors.toSet()));

            writer.write("public record %s(\n".formatted(entity.getName()));

            var first = true;
            for(var rel : entity.getOutgoingRelationships()) {
                if(!first) {
                    writer.write(",\n");
                } else {
                    first = false;
                }

                switch (rel.cardinality()) {
                    case ZERO_ONE, ONE ->
                        writer.write("    %s: %s".formatted(rel.name(),rel.target().getName()));
                    case ZERO_MANY, ONE_MANY ->
                        writer.write("    %s: List<%s>".formatted(rel.name(),rel.target().getName()));
                }
            }
            writer.write("\n) {}\n");
    }

    private void writeImports(Writer writer, Set<Type> importedTypes) throws IOException {
        for(var type : importedTypes) {
            if(type.getName().equals("LocalDate")) {
                writer.write("import %s.%s;\n".formatted( "java.time", type.getName()));
            }
        }
    }
}
