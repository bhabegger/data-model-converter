package tech.habegger.datamodel.converter.java;

import tech.habegger.datamodel.converter.ModelReader;
import tech.habegger.datamodel.metamodel.Cardinality;
import tech.habegger.datamodel.metamodel.EntityType;
import tech.habegger.datamodel.metamodel.Model;
import tech.habegger.datamodel.metamodel.Type;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.time.LocalDate;
import java.util.Collection;
import java.util.Set;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.function.Predicate.not;

public class JavaModelReader implements ModelReader {
    private static final Set<String> SCALAR_TYPES = Set.of(
        String.class.getSimpleName(),
        LocalDate.class.getSimpleName()
    );

    static Logger LOGGER = Logger.getLogger(JavaModelReader.class.getSimpleName());
    @Override
    public Model read(String sourceLocation) {
        Model model = new Model();
        Set<Class<?>> javaClasses = findAllClassesUsingClassLoader(sourceLocation);
        javaClasses.stream().filter(Class::isEnum).forEach(clazz -> addEnumToModel(clazz, model));
        javaClasses.stream().filter(not(Class::isEnum)).forEach(clazz -> addClassToModel(clazz, model));
        return model;
    }

    private void addEnumToModel(Class<?> clazz, Model model) {
        model.createEnumType(clazz.getSimpleName(), Stream.of(clazz.getEnumConstants()).map(Object::toString).collect(Collectors.toList()));
    }

    private void addClassToModel(Class<?> clazz, Model model) {
        EntityType entityType = model.getOrCreateEntityType(clazz.getSimpleName());

        Field[] fields = clazz.getDeclaredFields();
        for(Field field : fields) {
            Class<?> fieldClass = field.getType();
            Cardinality cardinality = Cardinality.ZERO_ONE;
            if(isCollection(fieldClass)) {
                if(field.getGenericType() instanceof ParameterizedType parameterizedType) {
                    fieldClass = (Class<?>) parameterizedType.getActualTypeArguments()[0];
                    cardinality = Cardinality.ZERO_MANY;
                }
            }

            Type fieldType;
            if(fieldClass.isEnum()) {
                fieldType = model.getEnumType(fieldClass.getSimpleName());
            } else if(isScalarType(fieldClass)) {
                fieldType = model.getOrCreateScalarType(fieldClass.getSimpleName());
            } else {
                fieldType = model.getOrCreateEntityType(fieldClass.getSimpleName());
            }
            model.addRelationship(entityType, field.getName(), cardinality, fieldType);
        }
    }

    private boolean isCollection(Class<?> fieldClass) {
        return Collection.class.isAssignableFrom(fieldClass);
    }

    private boolean isScalarType(Class<?> fieldType) {
        return SCALAR_TYPES.contains(fieldType.getSimpleName());
    }


    // Code adapted from https://www.baeldung.com/java-find-all-classes-in-package
    public Set<Class<?>> findAllClassesUsingClassLoader(String packageName) {
        String resourceLocation = packageName.replaceAll("[.]", "/");
        InputStream stream = ClassLoader.getSystemClassLoader().getResourceAsStream(resourceLocation);
        if(stream == null) {
            LOGGER.info("No model found at %s".formatted(packageName));
            return Set.of();
        }
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
        return reader.lines()
                .filter(line -> line.endsWith(".class"))
                .map(line -> getClass(line, packageName))
                .collect(Collectors.toSet());
    }

    private Class<?> getClass(String className, String packageName) {
        try {
            return Class.forName(packageName + "." + className.substring(0, className.lastIndexOf('.')));
        } catch (ClassNotFoundException e) {
            // handle the exception
        }
        return null;
    }
}
