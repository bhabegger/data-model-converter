package tech.habegger.datamodel.sample;

public record Company(
        String name,
        Address address,
        CompanyType type
) {
}
