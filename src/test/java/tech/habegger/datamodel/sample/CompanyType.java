package tech.habegger.datamodel.sample;

public enum CompanyType {
    INDIVIDUAL,
    PRIVATE,
    LISTED
}
