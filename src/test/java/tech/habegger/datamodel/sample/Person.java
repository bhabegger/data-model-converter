package tech.habegger.datamodel.sample;

import java.time.LocalDate;
import java.util.List;

public record Person(
        String firstName,
        String lastName,
        LocalDate birthdate,
        Company employer,
        Address address,
        Person spouse,
        List<Person> children
) {
}
