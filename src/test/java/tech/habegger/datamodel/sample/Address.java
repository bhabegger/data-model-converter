package tech.habegger.datamodel.sample;

public record Address(
        String houseNumber,
        String street,
        String postalCode,
        String city,
        String country
) {
}
