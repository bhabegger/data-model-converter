package tech.habegger.datamodel.sandbox;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.networknt.schema.JsonSchema;
import com.networknt.schema.JsonSchemaFactory;
import com.networknt.schema.SpecVersion;
import com.networknt.schema.ValidationMessage;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class JsonValidatorTest {

    private final static ObjectMapper MAPPER = new ObjectMapper();
    @Test
    public void invalidInput() throws IOException {
        // Given
        JsonSchemaFactory factory = JsonSchemaFactory.getInstance(SpecVersion.VersionFlag.V4);
        JsonSchema jsonSchema = factory.getSchema(
                JsonValidatorTest.class.getResourceAsStream("schema.json"));
        JsonNode jsonNode = MAPPER.readTree(
                JsonValidatorTest.class.getResourceAsStream("invalid.json"));

        // When
        Set<ValidationMessage> errors = jsonSchema.validate(jsonNode);

        // Then
        assertThat(errors).isNotEmpty().asString().contains("price: must have a minimum value of 0");
    }

    @Test
    public void validInput() throws IOException {
        // Given
        JsonSchemaFactory factory = JsonSchemaFactory.getInstance(SpecVersion.VersionFlag.V4);
        JsonSchema jsonSchema = factory.getSchema(JsonValidatorTest.class.getResourceAsStream("/tech/habegger/datamodel/sample/json/person.json"));
        JsonNode jsonNode = MAPPER.readTree(JsonValidatorTest.class.getResourceAsStream("benjamin.json"));

        // When
        Set<ValidationMessage> errors = jsonSchema.validate(jsonNode);

        // Then
        assertThat(errors).isEmpty();
    }

}
