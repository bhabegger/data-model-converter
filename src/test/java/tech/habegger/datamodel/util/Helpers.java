package tech.habegger.datamodel.util;

import tech.habegger.datamodel.converter.java.JavaModelReader;
import tech.habegger.datamodel.metamodel.Model;

public class Helpers {
    public static Model readSampleJavaModel() {
        String packageName = "tech.habegger.datamodel.sample";
        JavaModelReader modelReader = new JavaModelReader();
        return modelReader.read(packageName);
    }
}
