package tech.habegger.datamodel.converter.java;

import org.junit.jupiter.api.Test;
import tech.habegger.datamodel.converter.graphql.GraphQLModelReader;
import tech.habegger.datamodel.metamodel.Model;

import java.io.File;
import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

class JavaModelWriterTest {

    GraphQLModelReader graphQLModelReader = new GraphQLModelReader();
    JavaModelWriter javaModelWriter = new JavaModelWriter();

    @Test
    void properlyGenerateClasses() throws IOException {
        // Given
        String resourceName = "/tech/habegger/datamodel/sample/sample.graphqls";
        Model model = graphQLModelReader.read(resourceName);

        // When
        javaModelWriter.write(model, "tech.habegger.sample");

        // Then
        File expected = new File("target/generated-src/main/java/tech/habegger/sample/Person.java");
        assertThat(expected).exists();
        assertThat(expected).hasContent("""
        package tech.habegger.sample;
                
        import java.time.LocalDate;
        public record Person(
            firstName: String,
            lastName: String,
            birthdate: LocalDate,
            employer: Company,
            address: Address,
            spouse: Person,
            children: List<Person>
        ) {}
        """);
    }
}