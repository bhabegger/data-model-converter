package tech.habegger.datamodel.converter.java;

import org.junit.jupiter.api.Test;
import tech.habegger.datamodel.metamodel.EntityType;
import tech.habegger.datamodel.metamodel.Relationship;

import java.util.Collection;

import static org.assertj.core.api.Assertions.assertThat;

class JavaModelReaderTest {

    JavaModelReader javaModelReader = new JavaModelReader();

    @Test
    public void classLoadingWorks() {
        // Given
        String packageName = "tech.habegger.datamodel.sample";

        // When
        var actual = javaModelReader.findAllClassesUsingClassLoader(packageName);

        // Then
        assertThat(actual.stream().map(Class::getSimpleName)).containsExactlyInAnyOrder(
            "Address", "Company", "Person", "CompanyType"
        );
    }

    @Test
    public void modelIsPopulated() {
        // Given
        String packageName = "tech.habegger.datamodel.sample";

        // When
        var model = javaModelReader.read(packageName);
        Collection<EntityType> entities = model.getEntityTypes();

        // Then
        assertThat(entities).hasSize(3);
        assertThat(entities.stream().map(EntityType::getName)).containsExactlyInAnyOrder(
                "Address", "Company", "Person"
        );
    }


    @Test
    public void entityHasAttributes() {
        // Given
        String packageName = "tech.habegger.datamodel.sample";

        // When
        var model = javaModelReader.read(packageName);
        var personEntityType = model.getEntityType("Person");
        var relationships = personEntityType.getOutgoingRelationships();

        // Then
        assertThat(relationships).hasSize(7);
        assertThat(relationships).allSatisfy(rel -> assertThat(rel.source()).isEqualTo(personEntityType));
        assertThat(relationships.stream().map(Relationship::name)).containsExactlyInAnyOrder(
            "firstName",
            "lastName",
            "birthdate",
            "employer",
            "address",
            "spouse",
            "children"
        );
    }
}