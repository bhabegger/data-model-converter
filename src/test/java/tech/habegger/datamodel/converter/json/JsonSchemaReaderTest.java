package tech.habegger.datamodel.converter.json;

import org.junit.jupiter.api.Test;
import tech.habegger.datamodel.converter.graphql.GraphQLModelWriter;
import tech.habegger.datamodel.metamodel.EntityType;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Collection;

import static org.assertj.core.api.Assertions.assertThat;

public class JsonSchemaReaderTest {

    JsonSchemaModelReader jsonModelReader = new JsonSchemaModelReader();
    @Test
    public void modelIsPopulated() {
        // Given
        String resourceName = "/tech/habegger/datamodel/sample/json/person.json";

        // When
        var model = jsonModelReader.read(resourceName);
        Collection<EntityType> entities = model.getEntityTypes();

        // Then
        assertThat(entities).hasSize(3);
        assertThat(entities.stream().map(EntityType::getName)).containsExactlyInAnyOrder(
                "Address", "Company", "Person"
        );
    }


    @Test
    public void conversionToGraphQL() throws IOException {
        // Given
        String resourceName = "/tech/habegger/datamodel/sample/json/person.json";
        var model = jsonModelReader.read(resourceName);

        // When
        GraphQLModelWriter graphQLWriter = new GraphQLModelWriter();
        StringWriter writer = new StringWriter();
        graphQLWriter.write(model, writer);

        // Then
        String graphqlSdl = writer.toString();
        assertThat(graphqlSdl).containsIgnoringWhitespaces("""
                type Person {
                    firstName: String
                    lastName: String
                    birthdate: LocalDate
                    employer: Company
                    address: Address
                    spouse: Person
                    children: [Person]
                }
                """);
    }
}
