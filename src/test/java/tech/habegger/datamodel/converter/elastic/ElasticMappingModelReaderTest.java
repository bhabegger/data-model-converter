package tech.habegger.datamodel.converter.elastic;

import org.junit.jupiter.api.Test;
import tech.habegger.datamodel.converter.graphql.GraphQLModelWriter;
import tech.habegger.datamodel.metamodel.Model;

import java.io.IOException;
import java.io.StringWriter;
import java.net.URL;

import static org.assertj.core.api.Assertions.assertThat;

class ElasticMappingModelReaderTest {

    @Test
    public void properlyGenerateModel() throws IOException {
        // Given
        ElasticMappingModelReader elasticModelReader = new ElasticMappingModelReader();
        URL elasticModelURL = ElasticMappingModelReaderTest.class.getResource("/tech/habegger/datamodel/sample/elastic/person.json");
        assert elasticModelURL != null;

        // When
        Model model = elasticModelReader.read(elasticModelURL.toExternalForm());
        GraphQLModelWriter graphQLModelWriter = new GraphQLModelWriter();
        StringWriter writer = new StringWriter();
        graphQLModelWriter.write(model, writer);

        // Then
        assertThat(writer.toString()).containsIgnoringWhitespaces("""
            type Person {
                firstName: String
                lastName: String
                birthdate: Date
                employer: PersonEmployer
                address: PersonAddress
            }

        """);

    }



}