package tech.habegger.datamodel.converter.elastic;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;
import tech.habegger.datamodel.metamodel.Model;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringWriter;

import static org.assertj.core.api.Assertions.assertThat;
import static tech.habegger.datamodel.util.Helpers.readSampleJavaModel;

class ElasticMappingModelWriterTest {

    @Test
    public void properlyGenerateModel() throws IOException {
        // Given
        Model model = readSampleJavaModel();
        ElasticMappingModelWriter elasticWriter = new ElasticMappingModelWriter()
            .embed("Person", "address")
            .embed("Person", "employer")
            .embed("Company", "address")
            .join("Person", "children")
            .join("Person", "spouse")
        ;

        // When
        String targetDir = "target/generated-src/resources/elastic/";
        elasticWriter.write(model, targetDir);

        // Then
        File personTemplateFile = new File(new File(targetDir), "person.json");
        StringWriter writer = new StringWriter();
        IOUtils.copy(new FileReader(personTemplateFile), writer);
        String personTemplate = writer.toString();
        assertThat(personTemplate).containsIgnoringWhitespaces("""
            {
                "template": {
                    "mappings": {
                    "properties": {
                        "person": {
                            "properties": {
                                "firstName": {
                                    "type": "keyword"
                                },
                                "lastName": {
                                    "type": "keyword"
                                },
                                "birthdate": {
                                    "type": "date"
                                },
                                "employer": {
                                    "properties": {
                                        "name": {
                                            "type": "keyword"
                                        },
                                        "address": {
                                            "properties": {
                                                "houseNumber": {
                                                    "type": "keyword"
                                                },
                                                "street": {
                                                    "type": "keyword"
                                                },
                                                "postalCode": {
                                                    "type": "keyword"
                                                },
                                                "city": {
                                                    "type": "keyword"
                                                },
                                                "country": {
                                                    "type": "keyword"
                                                }
                                            }
                                        }
                                    }
                                },
                                "address": {
                                    "properties": {
                                        "houseNumber": {
                                            "type": "keyword"
                                        },
                                        "street": {
                                            "type": "keyword"
                                        },
                                        "postalCode": {
                                            "type": "keyword"
                                        },
                                        "city": {
                                            "type": "keyword"
                                        },
                                        "country": {
                                            "type": "keyword"
                                        }
                                    }
                                },
                                "__relationships": {
                                    "type": "join",
                                    "relations": {
                                        "children": "children_inv",
                                        "spouse": "spouse_inv"
                                    }
                                }
                            }
                        }
                    }
                }
            }
            """
        );
    }



}