package tech.habegger.datamodel.converter.graphql;

import org.junit.jupiter.api.Test;
import tech.habegger.datamodel.metamodel.EntityType;
import tech.habegger.datamodel.metamodel.Relationship;

import java.io.IOException;
import java.util.Collection;

import static org.assertj.core.api.Assertions.assertThat;

class GraphQLModelReaderTest {

    GraphQLModelReader graphQLModelReader = new GraphQLModelReader();

    @Test
    public void modelIsPopulated() throws IOException {
        // Given
        String resourceName = "/tech/habegger/datamodel/sample/sample.graphqls";

        // When
        var model = graphQLModelReader.read(resourceName);
        Collection<EntityType> entities = model.getEntityTypes();

        // Then
        assertThat(entities.stream().map(EntityType::getName)).hasSize(3);
        assertThat(entities.stream().map(EntityType::getName)).containsExactlyInAnyOrder(
                "Address", "Company", "Person"
        );
    }


    @Test
    public void entityHasAttributes() throws IOException {
        // Given
        String resourceName = "/tech/habegger/datamodel/sample/sample.graphqls";

        // When
        var model = graphQLModelReader.read(resourceName);
        var personEntityType = model.getEntityType("Person");
        var relationships = personEntityType.getOutgoingRelationships();

        // Then
        assertThat(relationships).hasSize(7);
        assertThat(relationships).allSatisfy(rel -> assertThat(rel.source()).isEqualTo(personEntityType));
        assertThat(relationships.stream().map(Relationship::name)).containsExactlyInAnyOrder(
            "firstName",
            "lastName",
            "birthdate",
            "employer",
            "address",
            "spouse",
            "children"
        );
    }
}