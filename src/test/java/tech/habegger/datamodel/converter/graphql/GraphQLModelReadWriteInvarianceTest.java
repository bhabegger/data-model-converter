package tech.habegger.datamodel.converter.graphql;

import graphql.language.*;
import graphql.schema.idl.TypeDefinitionRegistry;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

class GraphQLModelReadWriteInvarianceTest {

    GraphQLModelReader graphQLModelReader = new GraphQLModelReader();
    GraphQLModelWriter graphQLModelWriter = new GraphQLModelWriter();

    @Test
    public void generatedSchemaIsSameAsInputSchema() throws IOException {
        // Given
        String resourceName = "/tech/habegger/datamodel/sample/sample.graphqls";
        TypeDefinitionRegistry originalSchema = GraphQLModelReader.schemaOfResource(resourceName);
        StringWriter writer = new StringWriter();

        // When
        graphQLModelWriter.write(graphQLModelReader.read(originalSchema), writer);

        // Then
        TypeDefinitionRegistry generatedSchema = GraphQLModelReader.schemaOfString(writer.toString());
        assertThat(generatedSchema).satisfies(equivalenceCheck(originalSchema));
    }

    private static Consumer<TypeDefinitionRegistry> equivalenceCheck(TypeDefinitionRegistry original) {
        return (toCheck) -> {
            assertThat(toCheck.types().keySet()).containsExactlyInAnyOrderElementsOf(original.types().keySet());
            assertThat(toCheck.types().values()).allSatisfy(
                    (definitionToCheck) -> equivalenceCheck(original.getType(definitionToCheck.getName()).orElseThrow(), definitionToCheck)
            );
        };
    }

    private static void equivalenceCheck(TypeDefinition<?> toCheck, TypeDefinition<?> original) {
        assertThat(toCheck.getClass()).isEqualTo(original.getClass());
        if(toCheck instanceof ObjectTypeDefinition toCheckObject && original instanceof ObjectTypeDefinition originalObject) {
            Map<String, FieldDefinition> originalFields = originalObject.getFieldDefinitions().stream()
                    .collect(Collectors.toMap(FieldDefinition::getName, x -> x));
            assertThat(toCheckObject.getFieldDefinitions())
                .allSatisfy(toCheckField -> {
                        var originalField = originalFields.get(toCheckField.getName());
                        assertThat(originalField).isNotNull();
                        equivalenceCheck(toCheckField.getType(), originalField.getType());
                });
        }
    }

    private static void equivalenceCheck(Type<?> toCheck, Type<?> original) {
        assertThat(toCheck.getClass()).isEqualTo(original.getClass());
        if(toCheck instanceof NonNullType toCheckNonNull && original instanceof NonNullType originalNonNull) {
            equivalenceCheck(toCheckNonNull.getType(), originalNonNull.getType());
        } else if(toCheck instanceof ListType toCheckList && original instanceof ListType originalList) {
            equivalenceCheck(toCheckList.getType(), originalList.getType());
        } else if (toCheck instanceof TypeName toCheckName && original instanceof TypeName originalName){
            assertThat(toCheckName.getName()).isEqualTo(originalName.getName());
        }
    }
}