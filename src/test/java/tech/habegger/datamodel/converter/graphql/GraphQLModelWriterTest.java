package tech.habegger.datamodel.converter.graphql;

import org.junit.jupiter.api.Test;
import tech.habegger.datamodel.metamodel.Cardinality;
import tech.habegger.datamodel.metamodel.EntityType;
import tech.habegger.datamodel.metamodel.Model;
import tech.habegger.datamodel.util.Helpers;

import java.io.IOException;
import java.io.StringWriter;

import static org.assertj.core.api.Assertions.assertThat;

class GraphQLModelWriterTest {

    @Test
    public void properlyGenerateModel() throws IOException {
        // Given
        Model model = Helpers.readSampleJavaModel();
        GraphQLModelWriter graphQLWriter = new GraphQLModelWriter();

        // When
        StringWriter writer = new StringWriter();
        graphQLWriter.write(model, writer);

        // Then
        String graphqlSdl = writer.toString();
        assertThat(graphqlSdl).containsIgnoringWhitespaces("""
                type Person {
                    firstName: String
                    lastName: String
                    birthdate: LocalDate
                    employer: Company
                    address: Address
                    spouse: Person
                    children: [Person]
                }
                """);
    }

    @Test
    public void properlyHandleEnums() throws IOException {
        // Given
        Model model = Helpers.readSampleJavaModel();
        GraphQLModelWriter graphQLWriter = new GraphQLModelWriter();

        // When
        StringWriter writer = new StringWriter();
        graphQLWriter.write(model, writer);

        // Then
        String graphqlSdl = writer.toString();
        assertThat(graphqlSdl).containsIgnoringWhitespaces("""
                type Company {
                    name: String
                    address: Address
                    type: CompanyType
                }
                """);

        assertThat(graphqlSdl).containsIgnoringWhitespaces("""
                enum CompanyType {
                    INDIVIDUAL
                    PRIVATE
                    LISTED
                }
                """);
    }

    @Test
    public void onlyOutputValidGraphQLNames() throws IOException {
        // Given
        Model model = new Model();
        model.getOrCreateScalarType("Some-Number");
        EntityType type = model.getOrCreateEntityType("Super-Hero");
        model.addRelationship(type, "first-name", Cardinality.ONE, model.getOrCreateScalarType("String"));

        // When
        GraphQLModelWriter graphQLWriter = new GraphQLModelWriter();
        StringWriter writer = new StringWriter();
        graphQLWriter.write(model, writer);

        // Then
        String graphqlSdl = writer.toString();
        assertThat(graphqlSdl).containsIgnoringWhitespaces("""
                scalar Some_Number
                type Super_Hero {
                    first_name: String!
                }
                """);
    }
}