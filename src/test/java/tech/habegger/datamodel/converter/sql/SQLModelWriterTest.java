package tech.habegger.datamodel.converter.sql;

import org.apache.ibatis.jdbc.ScriptRunner;
import org.junit.jupiter.api.Test;
import tech.habegger.datamodel.metamodel.Model;
import tech.habegger.datamodel.util.Helpers;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.sql.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

public class SQLModelWriterTest {

    @Test
    public void properlyGenerateModel() throws IOException {
        // Given
        Model model = Helpers.readSampleJavaModel();
        SQLModelWriter sqlModelWriter = new SQLModelWriter();

        // When
        StringWriter writer = new StringWriter();
        sqlModelWriter.write(model, writer);

        // Then
        String sql = writer.toString();
        assertThat(sql).containsIgnoringWhitespaces(
        """
                CREATE TABLE person (
                    id INTEGER NOT NULL,
                    firstname VARCHAR(256),
                    lastname VARCHAR(256),
                    birthdate DATE,
                    employer INTEGER,
                    address INTEGER,
                    spouse INTEGER,
                    PRIMARY KEY (id)
                 );
                """);

        assertThat(sql).containsIgnoringWhitespaces(
        """
                ALTER TABLE person ADD CONSTRAINT fk_person_employer FOREIGN KEY (employer) REFERENCES company(id);
                ALTER TABLE person ADD CONSTRAINT fk_person_address FOREIGN KEY (address) REFERENCES address(id);
                ALTER TABLE person ADD CONSTRAINT fk_person_spouse FOREIGN KEY (spouse) REFERENCES person(id);
                """
        );

        assertThat(sql).containsIgnoringWhitespaces(
        """
                CREATE TABLE person_children (
                    children_in INTEGER NOT NULL,
                    children_out INTEGER NOT NULL,
                    PRIMARY KEY (children_in, children_out)
                );
                """);

        assertThat(sql).containsIgnoringWhitespaces(
            """
                ALTER TABLE person_children ADD CONSTRAINT fk_person_children_children_in FOREIGN KEY (children_in) REFERENCES person(id);
                ALTER TABLE person_children ADD CONSTRAINT fk_person_children_children_out FOREIGN KEY (children_out) REFERENCES person(id);
                """
        );
    }

    @Test
    void generatedSchemaIsValid() throws IOException {
        // Given
        Model model = Helpers.readSampleJavaModel();
        StringWriter writer = new StringWriter();
        SQLModelWriter sqlModelWriter = new SQLModelWriter();
        sqlModelWriter.write(model, writer);
        String schemaScript = writer.toString();

        // When / Then
        var jdbcUrl = "jdbc:h2:mem:testdb";
        assertDoesNotThrow(() -> {
            try {
                try (Connection connection = DriverManager.getConnection(jdbcUrl)) {
                    ScriptRunner scriptRunner = new ScriptRunner(connection);
                    scriptRunner.setSendFullScript(false);
                    scriptRunner.setStopOnError(true);
                    scriptRunner.runScript(new StringReader(schemaScript));
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        });
    }
}
