# Data Model Converter

## Goal

### Main goal

The goal of this library is to provide a light weight tool for domain model conversion between commonly use data representations, among which:

 | **Model Format** | **Read support** | **Write Support** |
 |------------------|------------------|-------------------|
 | Java POJOs       | Basic            | Basic             |
 | GraphQL Schema   | Basic            | Basic             |
 | JSON Schema      | No               | No                |
 | XML Schema       | No               | No                |
 | SQL DB Schema    | No               | No                |
 | Elastic Mappings | Basic            | No                |

As this project is still in it's very beginning and experimental phase, you can see that support is still quite limited

### Secondary goal

The secondary goal of this project is to make explicit and visible the differences in expressivity each of the modeling approaches has.

## Supported constructs

* Class / Entity / Type
* Scalar (aka values vs entities)
* Relationship / Association / Field / Parameter / Attribute
* Cardinality
  * Java Collections as multiple cardinality


### Challenges

#### Elastic Generation

* When to choose keyword or text ?
* How to handle relationships ?
  * Example cases:
    * Person -> address -> Address
    * Person -> employer -> Company
    * Person -> spouse -> Person
    * Person -> children -> Person
  * Possible options:
    * Just reference an "id": 
      * need to know the id field
      * will require application side joins
    * Denormalization: 
      * attention to "looping" references 
    * Nested field:
      * attention to "looping"
    * Join fields
      * How to name the join field ?
      * How to name both ends of the relations ?
  